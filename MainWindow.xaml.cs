﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Packaging;
using MahApps.Metro.Controls;
using System.IO;
using System.CodeDom.Compiler;
using System.CodeDom;

namespace Rouvier_Hugo_ST2TRD_TP2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // the "??" operator checks for nullability and value all at once.
            // because ConvertCheckBox.IsChecked is of type __bool ?__ which
            // is a nullable boolean, so it can either be true, false or null
            var toDecrypt = Decrypt.IsOn;
            var inputText = InputTextBox.Text;
            var inputKey = InputTextBox_Key.Text;
            var encryptionmethod = EncryptionComboBox.Text;

            if (!inputKey.All(char.IsDigit) && toDecrypt && encryptionmethod != "Vigenere")
            {
                MessageBox.Show("Please use a number for the key !");
            }
            else
            {
                if (toDecrypt)
                {
                    OutputTextBox.Text = $"{inputText} is gibberish and should be decrypted using {encryptionmethod}";

                }
                else
                {
                    OutputTextBox.Text = $"{inputText} was written as an input to be encrypted using {encryptionmethod}";
                }

                if (encryptionmethod == "Caesar")
                {
                    OutputTextBox.Text = Caesar.Code(inputText, toDecrypt, inputKey);
                }
                if (encryptionmethod == "XoR")
                {
                    if (!inputKey.All(char.IsDigit))
                    {
                        MessageBox.Show("Please use a number for the key !");
                    }
                    else
                    {
                        OutputTextBox.Text = XORCipher.EncryptOrDecrypt(inputText,inputKey, toDecrypt);
                    }
                    
                }
                if (encryptionmethod == "Vigenere")
                {
                    if (!inputKey.All(char.IsLetter))
                    {
                        MessageBox.Show("Please use only letters for the key !");
                    }
                    else
                    {
                        OutputTextBox.Text =  Vigenere.Code(inputText,toDecrypt,inputKey);
                    }

                }
            }
           
        }

        private void InputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void ToggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn)
                {
                    progress.IsActive = true;
                    progress.Visibility = Visibility.Visible;
                }
                else
                {
                    progress.IsActive = false;
                    //progress.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void text_input_key(object sender, TextCompositionEventArgs e)
        {

    
        }

       

        private void click(object sender, MouseEventArgs e)
        {
            InputTextBox_Key;
        }
    }
    internal static class Caesar
    {
        public static string Code(string inputText, bool toDecrypt, string inputKey)
        {
            // Ternary operator - Google it
            return toDecrypt ? Decrypt(inputText,inputKey) : Encrypt(inputText);
        }

        private static string Encrypt(string inputText)
        {
            string output = string.Empty;
            Random rnd = new();
            int key = rnd.Next(1, 25); 
            foreach (char ch in inputText)
            {
                output += Cipher(ch, key);
            }
                
            return $"{inputText} encrypted with Caesar !\nThe encrypted message is : {output}\nYour key to decrypt is : {key}";
        }

        public static char Cipher(char ch, int key)
        {
            if (!char.IsLetter(ch))
            {

                return ch;
            }

            char d = char.IsUpper(ch) ? 'A' : 'a';
            return (char)((((ch + key) - d) % 26) + d);
        }

        private static string Decrypt(string inputText, string inputKey)
        {
            int key = Int32.Parse(inputKey);
            string decryptMessage = Encipher(inputText, 26 - key);
            return $"{inputText} was decrypted with Caesar !\nAnd the message is : {decryptMessage}";
        }

        public static string Encipher(string input, int key)
        {
            string output = string.Empty;

            foreach (char ch in input)
            {
                output += Cipher(ch, key);
            }
                

            return output;
        }

    }

    internal static class XORCipher
    {

        public static string EncryptOrDecrypt(string text, string key, bool mode)
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                //for each char access the text nth character and apply the XOR operand on it by using the nth key character.

                result.Append((char)(text[c] ^ (uint)key[c % key.Length]));

            return $"{text} was decrypted with XoR !\nAnd the message is : " + result.ToString();
        }

        private static string ToLiteral(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    if (writer.ToString().Contains("\a"))
                    {
                        return writer.ToString().Replace("\a", "salam");
                    }
                    else
                    {
                        return "ok";
                    }
                }
            }
        }
       
    }

    internal static class Vigenere
    {
        // This function generates the key in
        // a cyclic manner until it's length isi'nt
        // equal to the length of original text

        public static string Code(string inputText, bool toDecrypt, string inputKey)
        {
            // Ternary operator - Google it
            return toDecrypt ? originalText(inputText, generateKey(inputText, inputKey)) : cipherText(inputText, generateKey(inputText,inputKey));
        }
        public static string generateKey(string str, string key)
        {
            int x = str.Length;

            for (int i = 0; ; i++)
            {
                if (x == i)
                    i = 0;
                if (key.Length == str.Length)
                    break;
                key += (key[i]);
            }
            return key;
        }

        // This function returns the encrypted text
        // generated with the help of the key
        public static string cipherText(string str, string key)
        {
            string cipher_text = "";

            for (int i = 0; i < str.Length; i++)
            {
                if (char.IsLetter(str[i]) is true)
                {
                    // converting in range 0-25
                    int x = (str[i] + key[i]) % 26;

                    // convert into alphabets(ASCII)
                    x += 'A';

                    cipher_text += (char)x;
                }

                else
                {
                    cipher_text += ' ';
                }
            }

            return $"{str} was encrypted with Vigenere !\nAnd the message is : " + cipher_text;
        }

        // This function decrypts the encrypted text
        // and returns the original text
        public static string originalText(string cipher_text, string key)
        {
            string orig_text = "";

            for (int i = 0; i < cipher_text.Length &&
                                    i < key.Length; i++)
            {
                if (char.IsLetter(cipher_text[i]) is true)
                {
                    // converting in range 0-25
                    int x = (cipher_text[i] -
                            key[i] + 26) % 26;

                    // convert into alphabets(ASCII)
                    x += 'A';
                    orig_text += (char)(x);
                }
                else
                {
                    orig_text += ' ';
                }

            }
            return $"{cipher_text} was decrypted with Vigenere !\nAnd the message is : " + orig_text;
        }
    }

}

